package labyrinth;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.GridDirection;
import datastructure.Location;


public class LabyrinthAI {

   

    /**
     * Makes a move in a random direction
     */
    public GridDirection step(ILabyrinth labyrinth) {
       return checkNeighbour(labyrinth);
       
    }

    public GridDirection checkNeighbour(ILabyrinth labyrinth) {
        List<GridDirection> legalDirections = new ArrayList<>();
        Location currentLoc = labyrinth.getPlayerLoc();
        Location nextLoc;
        List<GridDirection> directions = GridDirection.FOUR_DIRECTIONS;
        for (GridDirection d :directions) {
            if (!labyrinth.playerCanGo(d)) continue;
            nextLoc = currentLoc.getNeighbor(d);
            LabyrinthTile tile = labyrinth.getCell(nextLoc);

            if (tile==LabyrinthTile.MONSTER) continue;
            if (tile==LabyrinthTile.GOLD) {
                return d;
            }
            legalDirections.add(d);
        }
        if (legalDirections.size()>0) {
            Random r = new Random();
            GridDirection d = legalDirections.get(r.nextInt(legalDirections.size()));
            return d;
        }
        return getRandomDirection(labyrinth);
    }

    public GridDirection getRandomDirection(ILabyrinth labyrinth) {
        Random r = new Random();
        List<GridDirection> directions = GridDirection.FOUR_DIRECTIONS;
        GridDirection d = directions.get(r.nextInt(directions.size()));
        return d;
    }
    
}
